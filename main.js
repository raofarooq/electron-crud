console.log('main process working');

const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require("path");
const url = require("url");

let winMain , winEdit , winCreate;

//code for main window

function createMainWindow() {
    winMain = new BrowserWindow({width:1000 , height:800});
    
    winMain.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }))



    winMain.on('closed', () => {
        win = null;
    })


}
app.on('ready', createMainWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});


app.on('activate', () => {
    if (win === null) {
        createMainWindow()
    }
});

/*
//code for edit window

function createEditWindow() {
    winEdit = new BrowserWindow({width:1000 , height:800});
    

    winEdit.loadURL(url.format({
        pathname: path.join(__dirname, 'edit.html'),
        protocol: 'file',
        slashes: true
    }))
    
    winEdit.on('closed', () => {
        win = null;
    })
}
app.on('ready', createEditWindow);
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (win === null) {
        createEditWindow()
    }
});


//code for create window

exports.openWindow = (filename) =>
{

    winCreate = new BrowserWindow({width:1000 , height:800});
    

    winCreate.loadURL('file://${__dirname}/' + filename + '.html')

}


function CreateWindow() {
    
     winCreate = new BrowserWindow({width:1000 , height:800});
    

    winCreate.loadURL(url.format({
        pathname: path.join(__dirname, 'create.html'),
        protocol: 'file',
        slashes: true
    }))

    winCreate.on('closed', () => {
        win = null;
    })
}
app.on('ready', CreateWindow);
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (win === null) {
        CreateWindow()
    }
});
*/



